# Netlink-Resources

## Introduction

    This is a repository for following things:
    1. Swift User Sample
    2. Fin Troubleshooting Guides
    3. Vnc Connection Reset Script

## Access Level

    This is a public repository anyone within the workspace can pull and push to it.

### How to Guide

    To utilize the vnc connection reset script one should follow these steps:
    1. Clone the directory to your desired location
    2. Open terminal
    3. cd to "Vnc_Connection_Reset_Script"
    4. To make the script executable type "sudo chmod a+x vncreset.sh" and press enter
    5. Script is now executable type ./vncreset
    6. it'll run the script
    7. Enter Ip and credentials hit enter and your VNC connection is reset.
